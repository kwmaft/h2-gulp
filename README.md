# Hausübung 2
In der letzten Übung haben wir gemeinsam das Pipeline-Tool Gulp.js durchgenommen. 
Gulp ist nicht nur auf Frontend Pipelines beschränkt, sondern kann generell für 
Automatisierungen in einem Projekt verwendet werden.

### Aktueller Stand
Wir haben gemeinsam eine Pipeline geschaffen, die es ermöglicht Source Dateien von 
Produktions Dateien (distribution) zu unterscheiden. Dabei haben wir html Dateien kopiert
und css Dateien kompiliert (autoprefixer) und minifiziert (cssnano).

Diese Dateien können dann über einen Connect Server abgerufen werden (localhost:1337), 
der auch in der Lage ist, css und html Dateien automatisch neu zu laden, wenn diese
verändert werden.

### Hausaufgabe
Bis zur nächsten Einheit, soll die Pipeline erweitert werden. Sie soll in der Lage sein
Bild Dateien (png, jpeg, svg) aus dem Source Verzeichnis ebenfalls in das Dist Verzeichnis
zu kopieren. Dabei sollen die Bilder für das Web optimiert (komprimiert) werden.

Hilfreiche Links dafür:

- [https://www.npmjs.com/search?q=gulp%20image%20optimize](https://www.npmjs.com/search?q=gulp%20image%20optimize)
- [https://www.npmjs.com/package/gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin)
- [https://www.npmjs.com/package/gulp-image](https://www.npmjs.com/package/gulp-image)

### Bonusaufgabe
Oft reicht das Optimieren von Bildern für das Web jedoch nicht, da viele Webseiten noch
viel zu große Bilder verwenden (2000 Pixel breit, obwohl sie nie größer als 1080 Pixel
dargestellt werden). Als unverpflichtende Bonusaufgabe soll die Pipeline noch etwas 
erweitert werden. Dabei sollen Bilder nicht nur optimiert, sondern generell auch verkleinert werden.
Ein Beispiel dafür wäre, alle Bilder die breiter als 1080 Pixel sind auf 1080 Pixel Breite
zu beschränken. Es können natürlich auch verschieden Versionen erstellt werden, für die 
verschiedenen Bildschrimgrößen (Handy, Tablet, Desktop).

Hilfreiche Links dafür:

- [https://www.npmjs.com/package/gulp-responsive](https://www.npmjs.com/package/gulp-responsive)
    - [https://github.com/mahnunchik/gulp-responsive/blob/HEAD/examples/multiple-resolutions.md](https://github.com/mahnunchik/gulp-responsive/blob/HEAD/examples/multiple-resolutions.md)
- [https://www.npmjs.com/package/gulp-gm](https://www.npmjs.com/package/gulp-gm)