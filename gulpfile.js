const
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    clean = require('gulp-clean');

let env = 'prod';

/**
 * Config for path variables. E.g. differentiate between src and dist folder.
 */
let path = {
    // All source files reside inside the src folder.
    src: './src/',

    // All files ready for production are being copied / compiled into the distribution folder
    dist: './dist/',

    // All css files.
    css: "**/*.css",

    // All html files.
    html: "**/*.html",

    // All image files.
    images: "**/*.{png,jpg}"
};

/**
 * This task compiles css for production use by prefixing for cross browser compatibility, minifying and
 * creating source maps.
 */
gulp.task('css', function () {
    return gulp
    // Start a gulp stream for all css files inside the src folder
        .src([path.src + path.css])
        // Initialize the source maps
        .pipe(sourcemaps.init())
        // Prefix all css properties for all browsers
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        // Miniy the css.
        .pipe(gulpif(env === "prod", cssnano()))
        // Create the source maps in the same folder as the css file is compiled to.
        .pipe(sourcemaps.write("."))
        // Define the output folder (distribution).
        .pipe(gulp.dest(path.dist))
        // Reload the connect server for live reloading in the browser.
        .pipe(connect.reload());
});

/**
 * This task moves all html files from the src folder to the distribution folder.
 * Since no additional plugins are provided between src and gulp.dest the files are only being copied.
 */
gulp.task('moveHTML', () => {
    return gulp
    // Start a gulp stream for all html files inside the src folder
        .src([path.src + path.html])
        // Define the output folder (distribution)
        .pipe(gulp.dest(path.dist))
        // Reload the connect server for live reloading in the browser.
        .pipe(connect.reload())
});

/**
 * This task starts a small connect server, which is able to live reload after css, js or html changes.
 * Server will be running on localhost:1337.
 */
gulp.task('connect', () => {
    return connect.server({
        // Server root is the distribution folder.
        root: './dist',
        port: 1337,
        livereload: true
    })
});

/**
 * This task clears the distribution folder, removing old files which are not needed anymore.
 */
gulp.task('clean', () => {
    return gulp.src([path.dist], {
        allowEmpty: true
    })
        .pipe(clean())
});

/**
 * This task watches all css files inside the src folder and re-executes the css task.
 */
gulp.task('watch:css', () => {
    return gulp.watch([path.src + path.css], gulp.series('css'));
});

/**
 * This task watches all html files inside the src folder and re-executes the moveHTML task.
 */
gulp.task('watch:html', () => {
    return gulp.watch([path.src + path.html], gulp.series('moveHTML'));
});

/**
 * Default Task. This task can be started by executing gulp inside this directory without providing a task name.
 * Uses Gulp-Sequence to executes all necessary tasks in a given sequential order.
 */
gulp.task('default', gulp.series("clean", gulp.parallel("moveHTML", "css"),  gulp.parallel("watch:css", "watch:html", "connect")));


